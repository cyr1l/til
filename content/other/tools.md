---
title: "Tools"
date: 2018-01-28T13:48:36+01:00
description : ""
#draft: false
tags: ["tools"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

## DNS
* [WhoisThisDomain](http://www.nirsoft.net/utils/whois_this_domain.html) Domain name search tool (Whois) for Windows

## E-Mail SMTP
* [smtp4dev](http://smtp4dev.codeplex.com/downloads/get/269147) A little SMTP mail server that can redirect mails to a folder.

## Console
* [cmder](http://cmder.net/) A better and more beautiful console experiance for windows

## USB boot disk
* [Rufus](https://rufus.akeo.ie/) Tool to create boot disks on a usb drive
