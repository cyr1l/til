---
title: "Links"
date: 2018-01-28T13:37:03+01:00
description : ""
#draft: false
tags: 
- notag
---

## Links
* [Simple Checklist](http://secretgeek.net/simple_checklist) Simple Trouble Shooting Application Now Fixes Everything
* [Sysadmincast](https://sysadmincasts.com/episodes/25-bits-sysadmins-should-know) Bits Sysadmins Should Know
* [IT Landscape for sysadmins](http://sysadmin.it-landscape.info/) IT tools landscape (mostly linux and opensource)
* [Cloud Native Landscape](https://github.com/cncf/landscape) Cloud Native Landscape (currently most used cloud software)
* [RMM Comparison](http://Rmm.msp.zone)Compare different rmm tools
