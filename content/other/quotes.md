---
title: "Quotes"
date: 2019-07-29T23:04:05+02:00
description : ""
#draft: false
tags:
- quotes
---

> May there be peace in my heart,
> and strength in my soul.
> Wisdom in my thoughts,
> and courage in my actions.