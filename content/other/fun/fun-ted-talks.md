---
title: "Fun Ted Talks"
date: 2018-01-28T14:11:24+01:00
description : ""
#draft: false
tags: ["notag"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

* [This is what happens when you reply to spam email | James Veitch](https://www.youtube.com/watch?v=_QdPW8JrYzQ)
* [More adventures in replying to spam | James Veitch](https://www.youtube.com/watch?v=C4Uc-cztsJo)
