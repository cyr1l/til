---
title: "Powershell"
date: 2018-07-13T11:31:14+02:00
description : ""
#draft: false
tags: ["powershell"] #[ "Development", "Go", "Powershell", "Blogging" ]
---


Learning tools / exercises
* https://github.com/vexx32/PSKoans

* https://docs.microsoft.com/en-us/powershell/scripting/overview?view=powershell-6
* https://www.tutorialspoint.com/
* https://powershellexplained.com/tags/?utm_source=reddit&utm_medium=comment&utm_content=powershell_the_final_frontier
* https://powershell.org/free-resources/