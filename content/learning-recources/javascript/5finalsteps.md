---
title: "Final Steps"
date: 2018-06-05T11:18:35+02:00
description : ""
#draft: false
tags: ["javascript"] #[ "Development", "Go", "Powershell", "Blogging" ]
---
You've come a long way in the past three weeks. You've learned how to learn JavaScript, you've became aware of the the traps that lie in your way.

What remains now, is for you to get your hands dirty with code. Go do the work. Go and learn.

In this final email, you'll get a summary of the important lessons you've went through. You'll also get a PDF of the course to keep.

Let's begin by going through the four phases of learning JavaScript.

The four phases of learning JavaScript
--------------------------------------

-   **The Baby Phase** -- This is where you start off when you're new to JavaScript. Here, you should focus JavaScript's syntax so you know what are things like Strings, Arrays and Objects.
-   **The Child Phase** -- You should be familiar with the syntax here. You already how to use statements like `if` and `for`. You also know how to use Arrays and Objects. What's next for you is to master the DOM. Try building simple components here. Don't worry about code quality at the phase.
-   **The Teenage Phase** -- In the teenage phase, you focus on improving code quality while learning advanced topics like AJAX, OOP and FP. Best practices go to no end. Feel free to move into the adult phase whenever you feel that you're good enough to explore something else.
-   **The Adult Phase** -- Here, you know enough JavaScript to be dangerous. This is where you begin exploring things built on JavaScript. You may explore frontend frameworks (like React and Angular), backend (Node, Express and MongoDB), libraries (GSAP, D3, etc) or even dive deeper into learning JavaScript.

The key to learning JavaScript is to take step at a time. Don't skip steps while you're learning, because they'll leave you confused and overwhelmed.

The Traps
---------

Beware of the learning traps you set for yourself! Most learners face three self-defeating traps -- the victim trap, the learn fast trap and the paralysis trap.

**The Victim Trap**

You're in the victim trap if you feel that you're powerless about your circumstances. Something out there is preventing you from learning JavaScript. You don't have enough time, money nor energy. Requirements for jobs are too insane, etc.

Break yourself out of the victim trap by realizing you **you control your life**. Carve out time for JavaScript. Stop making excuses for yourself, no matter how valid they are.

Start doing the work. Stop playing the role of a victim.

**The Learn Fast Trap**.

You're in the learn fast trap if you feel the urge to learn quickly. You've set yourself an unreasonable deadline.

Break yourself out of the learn fast trap by **preparing yourself to learn well**, not fast.

Take it slow. Pay attention and try to formulate arguments from what you read. While you do so, drop any preconceived ideas you have about the subject.

Experiment. Code. Fail with your experiments. Start another. Do do it over and over, tweaking till you succeed.

The Paralysis Trap
------------------

You're in the paralysis trap if you feel you need to make the right decision before starting to learn.

Break yourself out of the paralysis trap by **making decisions despite not knowing if you're right**.

Don't worry if you're wrong. It makes a valuable lesson. You know what not to do from now on. Thomas Edison didn't fail to invent the lightbulb for 9,999 times. He found 9,999 ways that wouldn't work. That's why he made it the 10,000 time.

Here's a PDF of the course
--------------------------

3 weeks is almost too short for a course of this magnitude. You probably don't want to lose these emails in your sea of emails. I get you.

That's why I prepared a PDF version of the course. You can download it by clicking the link below:

[Download PDF of JavaScript Roadmap](https://jsroadmap.com/images/JSR.pdf)

Take this PDF. Put it somewhere you can see everyday. Use it to review the questions as you get through each phase. Use it to remind you of the traps you may face.

Most importantly, keep learning JavaScript. You can do it.

If you need extra help
----------------------

If you need extra help with JavaScript, you'll be glad to hear that I'm making a JavaScript course to help you out. It's called **Learn JavaScript.**

In **Learn JavaScript**, you'll get over 100 lessons to bring you through the baby, child and teenage phases of JavasScript Mastery. (You'd even learn some stuff from the adult phase). You'll also learn to build 21 things from scratch.

By the end of the course, you'll:

1.  Understand JavaScript so well that JavaScript tutorials out there no longer seem foreign. They become understandable. Native to you, even.
2.  Be able to build anything you want from scratch. No need to depend on plugins or modifying someone else's code.
3.  Remember JavaScript for life, even if you didn't touch it for three months.

Click on [this link](https://zellwk.com/learnjs/) if you're interested to hear more about Learn JavaScript.

If you need 1-1 help
--------------------

I also offer direct help like 1-1 mentoring and code reviews. If you're interested, feel free to hit reply and let me know.

Lastly, thank you.
------------------

Thank you for going through the JavaScript Roadmap. I hope it has helped you find your way better around JavaScript.

If this has helped you in any way, I'd love to hear about it. Please feel free to hit reply and let me know!

Finally, good luck learning JavaScript (and everything else you want!).