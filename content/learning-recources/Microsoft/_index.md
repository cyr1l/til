---
title: "Microsoft Learning resources"
description : ""
#tags: ["notag"] #[ "Development", "Go", "Powershell", "Blogging" ]
#pre: "<i class='fa fa-github'></i> "
---

{{% children depth="999" %}}

# Azure

## AZ-900 - Microsoft Azure Fundamentals

* https://www.microsoft.com/en-us/learning/exam-AZ-900.aspx
* https://docs.microsoft.com/en-us/learn/paths/azure-fundamentals/
* [Learn Azure in a Month of Lunches](https://azure.microsoft.com/en-us/resources/learn-azure-in-a-month-of-lunches/en-us/?OCID=AID681541_aff_7593_1243925&epi=lw9MynSeamY-tJ8X3Dhei4FRaOypZLPDXQ&irgwc=1&ranEAID=lw9MynSeamY&ranMID=24542&ranSiteID=lw9MynSeamY-tJ8X3Dhei4FRaOypZLPDXQ&tduid=%28ir__egl) (free e-book)

## AZ-100 - Microsoft Azure Infrastructure and Deployment

* https://www.microsoft.com/en-us/learning/exam-AZ-100.aspx
* https://openedx.microsoft.com/courses/course-v1:Microsoft+AZ-100.1+2019_T1/about
* https://openedx.microsoft.com/courses/course-v1:Microsoft+AZ-100.2+2019_T1/about
* https://openedx.microsoft.com/courses/course-v1:Microsoft+AZ-100.3+2019_T1/about
* https://openedx.microsoft.com/courses/course-v1:Microsoft+AZ-100.4+2019_T1/about
* https://openedx.microsoft.com/courses/course-v1:Microsoft+AZ-100.5+2019_T1/about
* https://msandbu.org/study-guide-for-azure-certifications-az-100-az-101/
* https://docs.microsoft.com/en-us/learn/browse/
* https://github.com/MicrosoftLearning/AZ-100-MicrosoftAzureInfrastructureDeployment
* https://www.learnondemandsystems.com/blog-azure-series/
* https://www.pluralsight.com/partners/microsoft/azure (free)
* https://courses.skylinesacademy.com/p/az-100

