---
title: "Coding"
date: 2018-01-28T14:07:11+01:00
description : ""
#draft: false
tags: ["notag"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

## online code running and testing
* https://repl.it/

## Free code courses
* https://www.freecodecamp.org/
* https://www.codecademy.com/
* https://www.sololearn.com/
* https://www.theodinproject.com/
* https://www.codeschool.com (Partly)
* https://www.theodinproject.com/
* https://www.freecodecamp.com
* https://en.hexlet.io/

## Exercises and challenges
* https://checkio.org/
* https://empireofcode.com/
* https://projecteuler.net/
* https://coderbyte.com/
* https://www.codewars.com/
* https://www.codingame.com
* https://edabit.com/challenges
* https://javascript30.com/
* https://medium.freecodecamp.org/6-absurd-ideas-for-building-your-first-web-application-24afca35e519
* https://www.fullstackacademy.com/student-gallery
* https://www.reddit.com/r/webdev/comments/3we48y/web_app_ideas_for_the_growing_web_developer/
* https://exercism.io/
* https://www.codechef.com/problems/easy/

### HTML CSS
* https://developer.mozilla.org/en-US/docs/Learn/HTML
* http://flexboxfroggasdy.com/#nl
* https://css-tricks.com/the-css-box-model/

### Databases
* https://www.pluralsight.com/blog/software-development/relational-non-relational-databases
* https://www.sitepoint.com/sql-vs-nosql-differences/
* https://university.mongodb.com/
* https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/mongoose
* https://blog.risingstack.com/node-js-database-tutorial/

### Git
* https://lab.github.com/
* https://codeburst.io/number-one-piece-of-advice-for-new-developers-ddd08abc8bfa
* https://codeburst.io/a-step-by-step-guide-to-making-your-first-github-contribution-5302260a2940
* https://github.com/jlord/git-it-electron#readme
* http://ericsteinborn.com/github-for-cats/#/
* http://opensourcerer.diy.org/