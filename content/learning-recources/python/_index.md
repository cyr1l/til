---
title: "Python"
description : ""
#tags: ["python"] #[ "Development", "Go", "Powershell", "Blogging" ]
#pre: "<i class='fa fa-github'></i> "
---

{{% children depth="999" %}}

* https://www.reddit.com/r/learnpython/wiki/index
* https://wiki.python.org/moin/BeginnersGuide/NonProgrammers
* [x] http://introtopython.org/
* https://automatetheboringstuff.com/
* https://www.datacamp.com/courses/intro-to-python-for-data-science

* https://runestone.academy/
* https://www.rithmschool.com/courses
* https://www.coursera.org/specializations/python
* https://pythonbasics.org/
* https://realpython.com/
* https://www.programiz.com/python-programming
* https://pythonprogramminglanguage.com/
* https://data-flair.training/blogs/python-tutorials-home/


* https://bitbucket.org/gregmalcolm/python_koans
* http://programarcadegames.com/