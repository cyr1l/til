!---
title: "Search engines"
date: 2019-11-11T10:49:19+02:00
description : ""
#draft: false
tags:
#- search
#- Development
#- Go
#- Powershell
#- Blogging
---

# Duck duck go

## Bangs

Use ```!r powershell``` to use the Reddit search engine.
Or use ```!tpw Philips Heu```  to search on Tweakers price watch for Philips Hue.

See [Duck duck Go Bangs](https://duckduckgo.com/bang).