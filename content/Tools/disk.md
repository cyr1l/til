---
title: "Disk Tools"
date: 2019-05-07T17:07:13+02:00
description : ""
#draft: false
tags: ["tools"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

## Disk space analize

### WizTree
https://antibody-software.com/web/software/software/wiztree-finds-the-files-and-folders-using-the-most-disk-space-on-your-hard-drive/

## S.M.A.R.T. 

### CrystalDiskInfo
https://crystalmark.info/en/software/crystaldiskinfo/

### Windows commandprompt
```bash
wmic diskdrive get status
```