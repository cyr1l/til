---
title: "Npm"
date: 2018-03-29T15:51:50+02:00
description : ""
#draft: false
tags: ["notag"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

## Start a project 
```
npm init
```

## install packages from package.json
```
npm install
```

## Install new package into project
```
npm install <package> --save
```
