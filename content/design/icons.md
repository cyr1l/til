---
title: "Icons"
date: 2018-01-28T13:52:33+01:00
description : ""
#draft: false
tags: ["notag"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

* [Illustrio](https://illustrio.com/) Big database of free icons
* [The Noun Project](https://thenounproject.com/) Big database release under Creative commons
