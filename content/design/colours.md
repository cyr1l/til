---
title: "Colours"
date: 2018-01-28T13:52:24+01:00
description : ""
#draft: false
tags: ["design", "colours"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

* [Paletton](http://paletton.com/) Generate nice colour palettes
* [Adobe Color CC](https://color.adobe.com/nl/) Adobe colour scheme chooser
* [Color Hunt](http://colorhunt.co/) The Pinterest for colours
* [Color Farm](http://color.farm/) Daily Color pallets based on popular Dribbble shots.
* [Open color](https://yeun.github.io/open-color/) A big opensource colourscheme
* [Color Claim](Color Claim) A collection of matching colours
* [Pantone Colors](Pantone Colors) collection of the Pantone colors
