---
title: "Filter and Compare"
date: 2018-01-29T12:30:19+01:00
description : ""
#draft: false
tags: ["notag"] #[ "Development", "Go", "Powershell", "Blogging" ]
weight: 6
---
## Filtering
```Powershell
Get-ADComputer -filter "Name -like '*DC'"
```


## Filter left
The best place to filter is in the first command.

## Where-Object
```Powershell
Get-Service | Where-Object -filter { $_.Status -eq 'Running' }
Get-Service | Where Status -eq 'Running'
Get-Service | Where Name -like '*print*'
Get-Process | where name -NotLike 'powershell*'
Get-Service | Where-Object -filter { $_.Status -eq 'Running' -and $_.StartType -eq 'manual' }
Get-Process | where {$_.ws -gt 10mb}
```


### Comparison Operators

* -eq - Equality
* -ne - Not Equal
* -ge and -el - Greater,less than or equal
* -gt and -lt - Greater than or less than
* Case sensitive versions -ceq, -cne, -cgt, -clt, -cge, -cle
* See other [Operators](http://til.ericq.net/powershell/beginning/operators/).
