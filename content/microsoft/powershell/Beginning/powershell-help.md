---
title: "Powershell Help"
date: 2018-01-29T12:28:55+01:00
description : ""
#draft: false
tags: ["powershell"] #[ "Development", "Go", "Powershell", "Blogging" ]
weight: 2
---
## Help

```Powershell
Get-Help Get-Service
```

Aliases `man` or `help` uses the more command


## Update Help
Powershell 3 and higher
```Powershell
Update-Help
```

Display the full help with -full
```Powershell
Help Get-EventLog -full
```

Display help in a separate window with -ShowWindow
```Powershell
Get-Help Get-Help -ShowWindow
```

## Get-Help
```Powershell
Get-Help Get-Process
Get-Help Get-Process -Online
```

## Find a command
Get a list of command
```Powershell
Get-Help *eventlog*
Get-Command *eventlog*
```

## About topics
Powershell has a lot of about topics

```Powershell
help about*
help about_break
help about_operators
```

## Show-Command
Display a form
```Powershell
Show-Command Get-Process
```


## Aliases
```Powershell
get-alias -Definition "Get-Service"
```
Parameter alias
```Powershell
(get-command get-eventlog | select -ExpandProperty parameters).comp
utername.aliases
```


## List all Object properties of a Collection
```Powershell
Get-Member
gm
Get-Process | Get-Member
```


## Update help
```Powershell
update-help
update-help -UICulture EN-US, nl-NL
```


## Powersehell ISE
Start powershell ISE for powershell
```Powershell
ise
ise .\testscript.ps1
```
Open file from within ISE
```Powershell
psedit .\testscript.ps1
```
