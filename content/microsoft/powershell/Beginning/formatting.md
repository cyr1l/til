---
title: "Formatting"
date: 2018-01-29T12:30:03+01:00
description : ""
#draft: false
tags: ["Powershell"] #[ "Development", "Go", "Powershell", "Blogging" ]
weight: 5
---
## Format-Table
Resize the table with
```Powershell
Get-Process | ft -AutoSize
Get-Process | Format-Table
```
Select properties
```Powershell
Get-Process | Format-Table -property *
Get-Process | Format-Table -property ID,Name,Responding -autoSize
Get-Process | Format-Table * -autoSize
```
Group By
```Powershell
Get-Service | Sort-Object Status | Format-Table -groupBy Status
```
Rename Column
```Powershell
Get-Service | Format-Table @{n='ServiceName';e={$_.Name}},Status,DisplayName
```

Select columns and calculate size
```Powershell
Get-Process | Format-Table Name, @{n='VM(MB)';e={$_.VM / 1MB -as [int]}} -autosize
```

## Format-List
```Powershell
Get-Process | Format-List
```

## Format-Wide
```Powershell
Get-Process | format-wide
Get-Process | fw -AutoSize
Get-Process | fw -Column 3
```

## Out-GridView
Create a pop-up with a grid, sorting grid an search box
```Powershell
Get-Process | Out-GridView
```
