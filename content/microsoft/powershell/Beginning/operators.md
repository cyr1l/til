---
title: "Operators"
date: 2018-01-29T12:30:45+01:00
description : ""
#draft: false
tags: ["notag"] #[ "Development", "Go", "Powershell", "Blogging" ]
weight: 9
---
## Comparison Operators
| Operator                 | Description                    | Example                |
|--------------------------|--------------------------------|------------------------|
| -eq (-ne)                | Equal (Not Equal)              | $a –eq 8               |
| -gt                      | Greater than                   | 10 –gt $b              |
| -ge                      | Greater than or equal          | 123 –ge 321            |
| -lt                      | Less than                      | $a –lt $b              |
| -le                      | Less than or equal             | $a –le $c              |
| -Like (-NotLike)         | Wildcard string comparison     | $name –like “*shell”   |
| -Match (-NotMatch)       | Regular expression comparison  | $name –match “shell$”  |
| -Contains (-NotContains) | Does an array contain a value? | $name –contains “jeff” |
| -In (-NotIn)             | Is a value in an array         | “jeff” –in $name       |
String comparisons can be made case sensitive (-ceq, -cne, -clike)


## Arithmetic Operators
| Operator | Description         | Example       |
|----------|---------------------|---------------|
| *        | Multiplication      | $a * 3        |
| /        | Division            | $size / 1024  |
| +        | Addition            | $a + $b       |
| –        | Minus (or negation) | $size – $used |
| %        | Modulo (remainder)  | 21%7          |


## Logical Operators
| Logical  | Description                                                             | Example                      |
|----------|-------------------------------------------------------------------------|------------------------------|
| -And     | All parts of the expression must be true                                | 4 –gt 1) –AND ( 10 –lt 100) |
|          |                                                                         | True                         |
| -Or      | Any part of the expression must be true                                 | (4 –gt 99) –OR ( 10 –lt 100) |
|          |                                                                         | True                         |
| -Xor     | Logical exclusive or. True when one expression is True and one is False | (4 –gt 1) –XOR ( 10 –lt 100) |
|          |                                                                         | False                        |
| -Not (!) | Logical Not                                                             | -Not (10 –ge 9)              |
|          |                                                                         | False                        |


## Assignment Operators
| Operator | Description                       | Example                          |
|----------|-----------------------------------|----------------------------------|
| =        | Assign a value                    | $a = 1                           |
| +=       | Add a new value to an existing    | $a+=5                            |
|          | value                             | Adds 5 to $a and update $a       |
|          |                                   | Same as $a = $a+5                |
| -=       | Subtract a value from an existing | $a-=5                            |
|          | value                             | Subtract 5 from $a and update $a |
|          |                                   | Same as $a = $a -5               |
| *=       | Multiply a value from an existing | $a*=3                            |
|          | value                             | Same as $a = $a *3               |
| /=       | Divide a value from an existing   | $a/=2                            |
|          | value                             | Same as $a = $a /2               |
| ++       | Increase the value by 1           | $a++                             |
| —        | Decrease the value by 1           | $a–                              |
