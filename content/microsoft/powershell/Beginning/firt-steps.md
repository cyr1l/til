---
title: "Firt Steps"
date: 2018-01-29T12:28:28+01:00
description : ""
#draft: false
tags: ["powershell"] #[ "Development", "Go", "Powershell", "Blogging" ]
weight: 1
---

Get powershell version

```Powershell
$PSVersionTable
```

Powershell out-*

```Powershell
Get-Service | Out-GridView
```
