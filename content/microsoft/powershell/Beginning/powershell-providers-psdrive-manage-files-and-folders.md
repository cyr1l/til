---
title: "Powershell Providers Psdrive Manage Files and Folders"
date: 2018-01-29T12:29:35+01:00
description : ""
#draft: false
tags: ["powershell"] #[ "Development", "Go", "Powershell", "Blogging" ]
weight: 3
---
## Set working directory
```Powershell
Set-Location -Path C:\Windows\
```


## PS providers
get loaded providers
```Powershell
Get-PSProvider
```

Get available PSDrives
```Powershell
Get-PSDrive

Alias                                  Alias
C                  85,15        137,93 FileSystem    C:\
Cert                                   Certificate   \
D                   0,33          0,15 FileSystem    D:\
E                 251,40        680,11 FileSystem    E:\
Env                                    Environment
Function                               Function
G                  44,33         29,71 FileSystem    G:\
H                                      FileSystem    H:\
HKCU                                   Registry      HKEY_CURRENT_USER
HKLM                                   Registry      HKEY_LOCAL_MACHINE
Variable                               Variable
WSMan                                  WSMan
```


## Create new PS drive
```Powershell
New-PSDrive Install FileSystem \\biz-dc1\Install
```


## Registry
```Powershell
cd hklm:
cd .\SYSTEM\CurrentControlSet\Services\Browser\
dir


    Hive: HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Browser


Name                           Property
----                           --------
Parameters                     MaintainServerList     : Auto
                               ServiceDll             : C:\WINDOWS\System32\browser.dll
                               ServiceDllUnloadOnStop : 1
Security                       Security : {1, 0, 20, 128...}
```

## Environment
```Powershell
$env:computername

dir env:
```
