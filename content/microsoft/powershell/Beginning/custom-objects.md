---
title: "Custom Objects"
date: 2018-01-29T12:31:16+01:00
description : ""
#draft: false
tags: ["notag"] #[ "Development", "Go", "Powershell", "Blogging" ]
weight: 11
---
## Examples
Get all processes and create a new object layout. Create a object Runtime by calculating the runtime from the start time.
```Powershell
get-process | where starttime |
Select ID,Name,Starttime,
@{Name="RunTime";Expression={(Get-Date) - $_.StartTime}} |
Sort RunTime
```
