---
title: "Core Commands"
date: 2018-01-29T12:31:05+01:00
description : ""
#draft: false
tags: ["notag"] #[ "Development", "Go", "Powershell", "Blogging" ]
weight: 10
---
## Read file content
```Powershell
Get-Content whathappend.log
#Alias cat, type, gc

cat file.txt | where {$_} # filter empty lines
```


### Last or first x items
```Powershell
Get-Content file.txt -head 20
Get-Content file.txt -tail 10
Get-Content file.txt -tail 1 -Wait #wait at the and and show new lines
```


## Search for folders and files
```Powershell
Get-Childitem
#alias dir, ls, gci

dir c:\ -File #Only files
dir c:\ -Directory #Only directories


dir c:\temp
dir c:\temp -recurse

#Search for a file or filder
dir \\chi-fp01\it\*.ps1 -recurse  #slower
dir \\chi-fp01\it -include *.ps1 -recurse  #bit faster
dir \\chi-fp01\it -filter *.ps1 -recurse  #double speed

# Find hidden files
dir C:\ -Hidden
```


## Measure
```Powershell
get-process | measure
get-process | measure workingset -sum -min -max -average
```  
