---
title: "Variables"
date: 2018-01-29T12:30:36+01:00
description : ""
#draft: false
tags: ["notag"] #[ "Development", "Go", "Powershell", "Blogging" ]
weight: 8
---

Use variable in text
```Powershell
$svc = Get-Service browser
"The $($svc.displayname) service is $($svc.status)"
```
