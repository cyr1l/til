---
title: "Psreadline"
date: 2018-01-29T12:32:46+01:00
description : ""
#draft: false
tags: ["notag"] #[ "Development", "Go", "Powershell", "Blogging" ]
---
----
Image at bottom does not work!!
----
----


This module replaces the command line editing experience in PowerShell.exe for versions 3 and up. It provides:

 - Syntax coloring
 - Simple syntax error notification
 - A good multi-line experience (both editing and history)
 - Customizable key bindings
 - Cmd and emacs modes (neither are fully implemented yet, but both are
   usable)
 - Many configuration options
 - Bash style completion (optional in Cmd mode, default in Emacs mode)
 - Bash/zsh style interactive history search (CTRL-R)
 - Emacs yank/kill ring
 - PowerShell token based "word" movement and kill
 - Undo/redo
 - Automatic saving of history, including sharing history across live
   sessions
 - "Menu" completion (somewhat like Intellisense, select completion with
   arrows) via Ctrl+Space


## Installation
You will need PowerShellGet. It is included in Windows 10 and [WMF5](http://go.microsoft.com/fwlink/?LinkId=398175). If you are using PowerShell V3 or V4, you will need to install [PowerShellGet](https://www.microsoft.com/en-us/download/details.aspx?id=49186).

```Powershell
(new-object Net.WebClient).DownloadString("http://psget.net/GetPsGet.ps1") | iex
```
After installing PowerShellGet, you can simply run
```Powershell
install-module PsReadLine
```

Edit your powershell profile. The file `C:\Users\[User]\Documents\WindowsPowerShell\profile.ps1` is used for all hosts (e.g. the ISE and powershell.exe). If you already have this file, then you should add the following:
```Powershell
if ($host.Name -eq 'ConsoleHost')
{
 Import-Module PSReadline
}
```
Alternatively, the file `C:\Users\[User]\Documents\WindowsPowerShell\Microsoft.PowerShell_profile.ps1` is for *powershell.exe* only. Using this file, you can simply add:
```Powershell
Import-Module PSReadLine
```
## Usage
Preview current key bindings Ctrl + Alt + Shift + /
```Powershell
Get-PSReadlineKeyHandler
```

Handy shortcuts

| Shortcut                    | Description                             |
|-----------------------------|-----------------------------------------|
| Ctrl + Left / Right         | Navigate trough words                   |
| Ctrl + Shift + Left / Right | Select words                            |
| Ctrl + Home                 | Delete from cursor to beginning of line |
| Ctrl + End                  | Delete from cursor to end of line       |
| Ctrl + A                    | Select all                              |
| Ctrl + C                    | Copy                                    |
| Ctrl + X                    | Cut                                     |
| Ctrl + Y / Z                | Redo / Undo (changes)                   |
| Ctrl + L                    | Clear screen                            |


## Command completion
Command completion with Ctrl + Space

![alt text](/image.png"Hover text")
![This is an image](/img/image.png)


microsoft/powershell/commands/psreadline/
