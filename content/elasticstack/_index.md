!---
title: "Elastic Stack"
tags:
#- _index
#- Development
#- Go
#- Powershell
#- Blogging
---

{{% children depth="999" %}}


# Learn

* [x] https://www.guru99.com/elk-stack-tutorial.html
* [x] https://logz.io/learn/complete-guide-elk-stack/
* [ ] https://fullstackgeek.blogspot.com/2019/03/introduction-to-elasticsearch-and-elk-stack.html
* https://www.tutorialspoint.com/elasticsearch/

* https://www.elastic.co/start
* https://www.elastic.co/guide/en/elastic-stack-get-started/current/get-started-elastic-stack.html
* https://www.elastic.co/guide/en/elasticsearch/reference/current/index.html
* https://www.elastic.co/videos/
* https://www.udemy.com/course/elasticsearch-7-and-elastic-stack/
* https://www.youtube.com/playlist?list=PL9ooVrP1hQOEzUhBIOXgz65mbmsGjyrw0
* https://www.youtube.com/channel/UCUaTRF8LDgE6rQoduLWsErA/featured
* https://www.youtube.com/channel/UCuQzHmGPs-ydOcNzbwfa9hw
* https://www.youtube.com/playlist?list=PLhLSfisesZItMosBx0csZGld0n2htxXMO
* https://www.youtube.com/playlist?list=PL34sAs7_26wOgpqMW_0_E95k9tq2VkMOZ

* https://devconnected.com/syslog-the-complete-system-administrator-guide/
* https://devconnected.com/monitoring-linux-logs-with-kibana-and-rsyslog/
* https://devconnected.com/the-definitive-guide-to-centralized-logging-with-syslog-on-linux/
* https://www.elastic.co/blog/how-to-centralize-logs-with-rsyslog-logstash-and-elasticsearch-on-ubuntu-14-04

## More reading

### Elastic Serach
* https://logz.io/blog/elasticsearch-tutorial/
* https://logz.io/blog/10-elasticsearch-concepts/
* https://logz.io/blog/elasticsearch-cheat-sheet/
* https://logz.io/blog/the-top-5-elasticsearch-mistakes-how-to-avoid-them/
* https://logz.io/blog/elasticsearch-queries/
* https://logz.io/blog/5-easy-ways-to-crash-elk/
* https://logz.io/blog/elasticsearch-mapping/
* https://logz.io/blog/elasticsearch-performance-tuning/

### Logstash
* https://logz.io/blog/logstash-tutorial/
* https://logz.io/blog/logstash-grok/
* https://logz.io/blog/logstash-plugins/
* https://logz.io/blog/5-logstash-filter-plugins/
* https://logz.io/blog/5-logstash-pitfalls-and-how-to-avoid-them/
* https://logz.io/blog/debug-logstash/
* https://logz.io/blog/filebeat-vs-logstash/
* https://logz.io/blog/logstash-pipelines/

### Kibana
* https://logz.io/blog/kibana-tutorial/
* https://logz.io/blog/custom-kibana-visualizations/
* https://logz.io/blog/perfect-kibana-dashboard/
* https://logz.io/blog/kibana-hacks/
* https://logz.io/blog/kibana-advanced/

### Beats
* https://logz.io/blog/beats-tutorial/
* https://logz.io/blog/filebeat-tutorial/
* https://logz.io/blog/network-log-analysis-packetbeat-elk-stack/
* https://logz.io/blog/metricbeat-elastic-stack-5-0/
* https://logz.io/blog/windows-event-log-analysis/
* https://logz.io/blog/monitor-service-uptime/
* https://logz.io/blog/linux-auditbeat-elk/
* https://logz.io/blog/configuring-elasticsearch-beats/
* https://logz.io/blog/filebeat-pitfalls/


### Others
* [Kafka as buffer](https://logz.io/blog/deploying-kafka-with-elk/)
* Or redis as buffer queing
* https://logz.io/blog/securing-elasticsearch-clusters/

### More to read
* https://logz.io/blog/10-resources-you-should-bookmark-if-you-run-your-own-elk-stack/
* https://logz.io/blog/the-cost-of-doing-elk-stack-on-your-own/

# Extras
* [Sexilog - VMware, Veeam](http://www.sexilog.fr/features/)
* [elasticflow - netflos, sflow](https://github.com/robcowart/elastiflow)
* SearchGuard - free security plugin with role-based access control and ssl/tls for node-to-node communication

# Alternatieven
* Graylog
* Grafana Loki
* Fluentd and Fluent Bit
* nxlog (for windows)
* https://logz.io/blog/fluentd-logstash/
* https://wazuh.com/

# Greylog 
* https://www.youtube.com/channel/UC2J-s-1PzRYPJIPS4REq5zQ/featured
* https://www.youtube.com/playlist?list=PL-u0lIkpcBp92zSxITCRQjqrf1niuB4UW
* 