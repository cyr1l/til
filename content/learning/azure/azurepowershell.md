---
title: "Azure Powershell"
date: 2019-05-27T22:08:54+02:00
description : ""
#draft: false
tags: ["azure", "powershell"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

## Terminology

## Install

```powershell
Install-Module -Name Az -AllowClobber
```

## Login

```powershell
Login-AzAccount
```

## Resources