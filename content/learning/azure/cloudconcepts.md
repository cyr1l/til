---
title: "Cloud Concepts"
date: 2019-05-05T17:40:38+02:00
description : ""
#draft: false
tags: ["cloud"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

## Cloud Computing Models
* Public cloud
* Private cloud
* Hybrid cloud

## Cloud Service Models
* IaaS - Infrastructure as a service
* SaaS - Software as a service
* PaaS - Platform as a service
* DaaS - Database as a service
* DaaS - Desktop as a service
* WaaS - Workstation as a service

## Scaling 
* Scale Vertical (Scale Up) - Add more recources to one machine (more CPU, memory, etc).
* Scale Horizental (Scale Out) - Add more machines.
* Elastic scaling - Scale up or scale out based on the load.
* Resiliency - Recover from failure, disaster or high availability.

