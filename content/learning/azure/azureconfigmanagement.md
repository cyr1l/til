---
title: "Azure Configuration Management"
date: 2019-05-21T20:32:31+02:00
description : ""
#draft: false
tags: ["azure"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

# Azure Config management

Use tools like

* Ansible
* Chef
* Puppet
* DSC
* Cloud-init
* Azure Custom Script Extension
* Packer
* Terraform
* Jenkins

https://docs.microsoft.com/nl-nl/azure/virtual-machines/windows/infrastructure-automation

## DSC
