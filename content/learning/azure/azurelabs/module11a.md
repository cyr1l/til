---
title: "Module 11 a"
date: 2019-06-12T14:21:44+02:00
description : ""
#draft: false
tags: #[ "Development", "Go", "Powershell", "Blogging" ]
---

## Module 11 - Governance and Compliance 	Role-Based Access Control

## Exercise 1: Configure delegation of provisioning and management of Azure resources by using built-in Role-Based Access Control (RBAC) roles and built-in Azure policies

* Create Azure Active Directory (AD) users and groups.
* Create Azure resource groups.
* Delegate management of an Azure resource group via a built-in RBAC role.
* Assign a built-in Azure policy to an Azure resource group.

Done via Azure portal.

## Exercise 2: Verify delegation by provisioning Azure resources as a delegated admin and auditing provisioning events

* Identify an available DNS name for an Azure VM deployment.
* Attempt an automated deployment of a policy non-compliant Azure VM as a delegated admin.
* Perform an automated deployment of a policy compliant Azure VM as a delegated admin.
* Review Azure Activity Log events corresponding to Azure VM deployments.

```powershell
Test-AzDnsAvailability -Location 'West Europe' -DomainNameLabel bizqitbv
```

Error code:
RequestDisallowedByPolicy
Message:
Resource 'server2' was disallowed by policy. Policy identifiers: '[{"policyAssignment":{"name":"Allowed locations","id":"/subscriptions/d8d3583d-f7b5-4f90-9625-30e15f42cdd6/resourceGroups/az123/providers/Microsoft.Authorization/policyAssignments/4c1064a25ea04aac80d3f298"},"policyDefinition":{"name":"Allowed locations","id":"/providers/Microsoft.Authorization/policyDefinitions/e56962a6-4747-49cd-b67b-bf8b01975c4c"},"policySetDefinition":{"name":"Allowed locations","id":"/subscriptions/d8d3583d-f7b5-4f90-9625-30e15f42cdd6/providers/Microsoft.Authorization/policySetDefinitions/0535e415-f386-47fa-afbf-96680d2670d2"}}]'.