---
title: "Module 11 b"
date: 2019-06-12T15:11:24+02:00
description : ""
#draft: false
tags: #[ "Development", "Go", "Powershell", "Blogging" ]
---

## Module 11 - Governance and Compliance 	Governance and Compliance

### Exercise 1: Implement Azure tags by using Azure policies and initiatives.

* Provision Azure resources by using an Azure Resource Manager template.
* Implement an initiative and policy that evaluate resource tagging compliance.
* Implement a policy that enforces resource tagging compliance.
* Evaluate tagging enforcement and tagging compliance.
* Implement remediation of resource tagging non-compliance.
* Evaluate effects of the remediation task on compliance.

policies created

Tagging tested
