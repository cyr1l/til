---
title: "Azure Resource Manager (ARM)"
date: 2019-05-27T09:32:15+02:00
description : "The basic administration of Azure."
#draft: false
tags: ["azure"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

## Terminology

* Resource - A managable item in Azure.
* Resource group - A container holding resources.
* Resource provider - A service that supplies recources you can deploy.
* ARM template - Azure Resource Manager template. A SJON template file to deploy resources.

## Resource Group

When creating a resource group you supply a location for its metadata about the resources. The resources in the resource group can have various locations.

* Resources can only be in one resource group.
* Resource groups can not be renamed.
* Resource groups can have resources of many types.
* Resource groups can have recources of many regions.
* Removing a resource group deletes all its content.
* Moving a resource to a different resource group the source and target resource groups gets locked for cahnges.

Some best practices

* All recources in a recource group should have the same lifecycle. if you renew the frontend servers and leave the database server you should place the database server in a different resource group.
* A resource can only be in one resource group.
* You can move [allmost all](https://docs.microsoft.com/en-us/azure/azure-resource-manager/resource-group-move-resources#services-that-can-be-moved) resources between resource groups.
* A resource group can be used for setting administrative rights.

## Resource Tags

Resource tags can be applied to Azure resources to organize resources by categories. Each tag can have a name and value. For example a tag named Environment with a value of Production or Development.

* Each resource or resource group can have maximum 15 tags.
* Tags applied to a resource group is not inherited by the resources in the group.

## Resource locks

You can set two lock levels to prevent accidental problems. Locks can be set on resources, resource groups and subscriptions. Locks are inherited by child resources.

* **delete lock** - Prevents Deletion.
* **Read-Only** - Prevents changes beeing made to resources.

## Powershell cmdlets

### Create resource group

```powershell
New-AzResourceGroup -name MachineGroup1 -Location 'West Europe'
```

### Resource groups

```powershell
Remove-AzResourceGroup -Name "ContosoRG01"
```

### Resources

```powershell
Remove-AzResource -ResourceId <resourceId>
```

### Resource locks Powershell

```powershell
New-AzResourceLock -LockName <lockName> -LockLevel CanNotDelete -ResourceGroupName <resourceGroupName>
Get-AzResourceLock
Remove-AzResourceLock -LockName <Name>
Get-AzResourceLock
```