---
title: "Azure Virtual Machines"
date: 2019-05-03T15:29:55+02:00
description : ""
tags: ["azure"] #[ "Development", "Go", "Powershell", "Blogging" ]
#pre: "<i class='fa fa-github'></i> "
---

{{% children depth="999" %}}

## Terminology

## Azure Virtual Machine creation checklist

Think about the following when create a new VM:

* Start with the network
* Name the VM
* Decide the location for the VM
* Determine the size of the VM
* Understanding the pricing model
* Storage for the VM
* Select an operating system

### Naming a VM

Azure VM names can have up to 15 charactars on a Windows VM and 64 charactars on a Linux VM.
A good convention is to include the following information in the name:

| Element | Example | Note |
| --- | --- | --- |
| Company |   |   |
| Location | uw (US West), uew (West Europe) | Identifies the region into which the resource is deployed |
| Product or Service | service | Identifies the product, application, or service that the resource supports |
| Operating system |   |   |
| Role | sql, web, messaging | Identifies the role of the associated resource |
| Environment | dev, prod, QA | Identifies the environment for the resource |
| Instance | 01, 02 | For resources that have more than one named instance (web servers, etc.) |

For example, *usc-webvmdev01* might represent the first development web server hosted in the US South Central location.

### Azure VM Location and pricing

#### Azure VM Location

See the available [Azure regios](https://azure.microsoft.com/nl-nl/global-infrastructure/regions/) and products per region.

#### Azure VM pricing

When Calculating Azure VM costs think about the following elements:

* **Compute Costs** - Compute costs are priced per Hour but billed on a per-minute basis. You are not billed if a machine is stopped or deallocated. The costs include the Windows license if selected. The Linux instances are cheaper.
* **Storage Costs** - Storage is charged separately from the VM. If a machine is stopped you are only charged for the storage. When running you also pay for transactions.
* **Networking Costs** - The Network Costs consists of different components Like Virtual IP, Loadbalancers, VPN Gateways etc.
* **Bandwidth Costs** - Ingres data is free. Outgooing data is free untill 5 GB. Costs between different virtual machines is free in the same zone. Data between different availability zones and regions is calculated.

There are two cost structures for Azure VM's:

* **Consumption-based** - With the consumption-based option, you pay for compute capacity by the second.
* **Reserved Virtual Machine Instances** - The [Reserved Virtual Machine Instances](https://docs.microsoft.com/en-us/azure/virtual-machines/windows/prepay-reserved-vm-instances) (RI) are payed in advance for one or three years. The payment is up front and you can get up to 72% discount. You only get discount on the VM instance and not the Windows license. You can [cancel a reservation](https://docs.microsoft.com/en-us/azure/billing/billing-azure-reservations-self-service-exchange-and-refund) with a 12% early termination fee.

### Virtual Machine Sizes

| Type | Example name | Purpose |
|---|---|---|
| A - Basic | A1 | Basic verion of the A series for testing and development. For build servers, code repositories, low-traffic websites and web applications, micro services and small databases. |
| A - Standard  | A1 v2 | General purpose version of the A series. Better CPU, Load-balancing, auto-scaling, Higher Disk IO, better availability. |
| B - Burstable  | B1S, B1MS | VM instances that can burst to full capacity of the CPU using credits. When no high utilization credits are build up. Use for testing and development, low-trafic web servers, small databases and micro services. |
| D - General Purpose  |   D2 v2, DS1 v2, D2 v3,  D2s v3, DC2s  | Build for enterprise applications. DS instances for premium (Nvme) storage. |
| E - Memory Optimized  | E2 v3, E2s v3 | High memory to CPU core ratio. ES instances for premium (Nvme) storage. For database servers, caches and in-memory analitycs. |
| F - CPU Optimized  |  F1,  F1s, F2s v2,  | High CPU core to memory ratio. FS instances for premium (Nvme) storage. For batch processing, web servers, analitics and gaming. |
| G - Godzilla  | G1, Gs1 | Very large instances More memory and CPU power Biggist machines.. For large databases, ERP, SAP and data warehousing. |
| H - High performance | H8, H8m, H16r, HB60rs  | High memory and compute power with no Hyperthreading. Optional high-troughput network interfaces (RDMA). For simulations, processing analysisor modeling computing. |
| L - Storage Optimized | L4, L8s v2 | High throughput, low latency, directly mapped local NVMe storage and large local disks. For NoSQL or Data warehousing. |
| M - Large Memory optimized | M8ms, M32ts, M32ls | Largest memory optimized virtual machines. Heavy in-memory workloads such as SAP HANA and large core counts. |
| N - GPU Optimized | NC6, NC6 v2, NC6 v3, NV6, ND6 | Ideal for compute and graphics-intensive workloads. Optional high-troughput network interfaces (RDMA). For simulation, deep learning, graphics rendering, video editing, gaming and remote visualization. |
| S - SAP HANA |  |  Specialized certified instances for SAP HANA. |

| Specializations | Example | Description |
|---|---|---|
| S | DSv2 | Premium Storage optio (Nvme). |
| M | A2m_v2 | Large memory configuration of instance type. |
| R | H16mr | Supports remote direct memory access (RDMA) |

More information on the different machine types:

* [Sizes](https://docs.microsoft.com/en-us/azure/virtual-machines/linux/sizes) and [Prices](https://azure.microsoft.com/en-us/pricing/details/virtual-machines/linux/) for Linux virtual machines in Azure
* [Sizes](https://docs.microsoft.com/en-us/azure/virtual-machines/windows/sizes) and [Prices](https://azure.microsoft.com/en-us/pricing/details/virtual-machines/windows/) for Windows virtual machines in Azure

#### Azure Compute Units ACUs (performance)

[Azure compute unit](https://docs.microsoft.com/nl-nl/azure/virtual-machines/windows/acu) CPU Performance benchmark created by microsoft. The A1 type has a ACU of 100. A ACU of 200 is twice as fast. Hyper-threaded VM types may give less performance (2:1) but give nested virtualization options.

### Virtual Machine Disks

All Azure virtual machines have at least two disks, a operating system disk and a temporary disk. Azure VM machines can have one or more data disks.

#### Operating System Disks

Every virtual machine has one attatched operating system disk. The OS disk has a pre-installed OS. This disk has a maximum capacity of 2,048 GiB. It’s registered as a SATA drive and labeled as the C: drive by default.

#### Temporary Disk

Every VM contains a temporary disk, which is not a managed disk. This disk is intended for  short-term storage for applications, swap or page files. Data on this disk may be lost during maintenance or reconfiguration.

* On Windows, the disk is labeled D: drive by default and used for pagefile.sys.
* On Linux, the disk is typically /dev/sdb and is formatted and mounted to /mnt by the Azure Linux Agent

#### Data disks

A data disk is a managed disk that is attached to a VM to store application data. Data disks are registered as SCSI drives and are labeled with a letter that you choose. Each disk has a maximum of 4095 gibibytes. The size of the VM determines how many data disks you can attatch and the type of storage you can use.

### Storage Options

You can attatch multiple storage disks to a vm up to 256 TB of storage per VM.

Azure offers two ways of sttoring disks:

* **Unmanaged disks** - The original method of storing disks. In an unmanaged disk, you manage the storage accounts that you use to store the virtual hard disk (VHD) files that correspond to your VM disks. VHD files are stored as page blobs in Azure storage accounts.
* **Managed Disks** - An Azure managed disk is a virtual hard disk (VHD). You can think of it like a physical disk in an on-premises server but, virtualized. Azure managed disks are stored as page blobs, which are a random IO storage object in Azure. We call a managed disk ‘managed’ because it is an abstraction over page blobs, blob containers, and Azure storage accounts. With managed disks, all you have to do is provision the disk, and Azure takes care of the rest. When you select to use Azure managed disks with your workloads, Azure creates and manages the disk for you. The available types of disks are Ultra Solid State Drives (SSD) (Preview), Premium SSD, Standard SSD, and Standard Hard Disk Drives (HDD).

See the different [disk types](https://docs.microsoft.com/en-us/azure/virtual-machines/windows/disks-types) with their speed and sizes. Smaller disks have less speed. The VM you attatch the disk also has to meet this speed requirements. The giving speeds ara maximum posible speeds on a shared platform.

### Supported operating systems

Azure provides a variety of OS images. In the Azure market there are even more images of various products and vendors. If you can't find a suitable OS image  you can create your own 64-bit image and upload it to Azure sorage to create an Azure VM.

#### Windows server software support

All Microsoft software must be licensed correctly. Azure Windows machines have license included per default. You can also [License Mobility](www.microsoft.com/licensing/software-assurance/license-mobility.aspx) and use Software Assurence licenses. See the [license FAQ](https://azure.microsoft.com/is-is/pricing/licensing-faq/) for other software license structures. Windows Server CALs are not required. RDS calls are needed!

#### Linux server support

Azure supports many [Linux distributions](https://docs.microsoft.com/en-us/azure/virtual-machines/linux/endorsed-distros) and versions including CentOS by OpenLogic, Core OS, Debian, Oracle Linux, Red Hat Enterprise Linux, and Ubuntu.