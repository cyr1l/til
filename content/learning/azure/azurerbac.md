---
title: "Azure Role Based Access Control"
date: 2019-06-12T09:25:52+02:00
description : ""
#draft: false
tags: ["azure"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

When working with a team of people in Azure, you may want to start using security and compliance settings. RBAC is capable to grant appropriate access to Azure AD users, groups and services. RBAC is configured by selecting a role (the definition of what action is allowed or denied)and a user, group or service principal. The selection can then be applied to the entire subscription, a resource group, or to specific resources. The configured rights are inherited.

Users and groups are sourced from Azure Active Directory, which is commonly populated with credentials from on-premises directories, such as Active Directory. Note that RBAC access that you grant at parent scopes is inherited at child scopes.

## Terminology

* RBAC - Role Based Access Control
* Access Control (IAM)

## RBAC Roles

A role is a collection of actions that can be performed on Azure resources. The three most used roles are Owner, Contributor and Reader.

* **Owner** - can manage everything, including changing access.
* **Contributor** - can manage everything except access.
* **Reader** - can only view.
* **[Built-in](https://docs.microsoft.com/en-us/azure/role-based-access-control/built-in-roles)** - the default built-in roles.

## Administrator Permissions

Using the Azure AD, you can designate separate administrators to serve different functions. Administrators can be designated in the Azure AD portal to perform tasks such as adding or changing users, assigning administrative roles, resetting user passwords, managing user licenses, and managing domain names.

The global administrator has access to all administrative features. By default, the person who signs up for an Azure subscription is assigned the global administrator role for the directory. Only global administrators can assign other administrator roles.

## Role Definitions

Each role is a set of properties defined in a JSON file. The roles are set in the values allowable permissions (Actions), denied permissions (NotActions), and scope (read access, etc.).

In this example the Contributor role means all (asterisk) actions, NotActions, and all (/) scopes. This information is available with the Get-AzureRmRoleDefinition cmdlet.

```
Name             : Contributor
Id               : b24988ac-6180-42a0-ab88-20f7382dd24c
IsCustom         : False
Description      : Lets you manage everything except access to resources.
Actions          : {*}
NotActions       : {Microsoft.Authorization/*/Delete, Microsoft.Authorization/*/Write, Microsoft.Authorization/elevateAccess/Action, Microsoft.Blueprint/blueprintAssignments/write...}
DataActions      : {}
NotDataActions   : {}
AssignableScopes : {/}
```

Role scopes can also get assignet to specific scopes like subscriptions, resource groups, or resources. You can create your own custom roles for assigments.

```
* /subscriptions/[subscription id]
* /subscriptions/[subscription id]/resourceGroups/[resource group name]
* /subscriptions/[subscription id]/resourceGroups/[resource group name]/[resource]
* “/subscriptions/c276fc76-9cd4-44c9-99a7-4fd71546436e”, “/subscriptions/e91d47c4-76f3-4271-a796-21b4ecfe3624” 
* “/subscriptions/c276fc76-9cd4-44c9-99a7-4fd71546436e/resourceGroups/Network”
```

## Powershell commands

List role assignments

```powershell
Get-AzRoleAssignment -ResourceGroupName grpname
```

List role definitions.

```powershell
Get-AzRoleDefinition | FT Name, Description
```


List the actions of the owner role.

```powershell
Get-AzRoleDefinition owner | FL Actions, NotActions
```

## Resources

* [Azure RBAC](https://docs.microsoft.com/en-us/azure/role-based-access-control/overview)
* [Built-in RBAC roles](https://docs.microsoft.com/en-us/azure/role-based-access-control/built-in-roles)
