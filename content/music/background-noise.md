---
title: "Background Noise"
date: 2018-01-28T14:05:19+01:00
description : ""
#draft: false
tags: ["notag"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

* [Mynoise](https://mynoise.net/) A extensive noise generator (includes iphone and android app)
* [Coffitivity](https://coffitivity.com/) Ambient sounds of a cafe
