---
title: "Cheat Sheet"
date: 2018-01-22T01:17:37+01:00
description : ""
draft: false
tags: ["notag"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

# Hugo Markdown Cheatsheet

## Text Formatting

### Headers

```text
# Heading 1
## Heading 2
### Heading 3
#### Heading 4
##### Heading 5

Alternative for H1 and H2 with underline

Alt-H1
======
Alt-H2
------
```

# Heading 1

## Heading 2

### Heading 3

#### Heading 4

##### Heading 5

Alternative for H1 and H2 with underline

Alt-H1
======

Alt-H2
------

### Basic text foramtting

```text
This *is italic*, and _also this_
This **is Stong**, and __also this is bold__
Combined **asterisks and _underscores_** or ***asdasdadsa***
~~Strikethrough~ uses two tildes
```

This *is italic*, and _also this_

This **is Stong**, and __also this is bold__

Combined **asterisks and _underscores_** or ***asdasdadsa***

~~Strikethrough~~ uses two tildes

### Lists

```text
A list is created using asterisks or dashes

* First
* Second
* Third
  * sub1
  * sub2
    * supper

Nested and numeric lists are possible

1. First
2. Second
    * Sub 1
    * Sub 2
```

A list is created using asterisks or dashes

* First
* Second
* Third
  * sub1
  * sub2
    * supper

Nested and numeric lists are possible

1. First
2. Second
    * Sub 1
    * Sub 2

### Links

```text
www.nu.nl
https://google.com
<http://google.com>
<kees@example.com>

[link to this](https://www.google.com)

[like this](http://someurl "this title shows up when you hover")

You can also put the [link URL][1] below the current paragraph
like [this][2]. Search the web [here][searchengine]

   [1]: http://url
   [2]: http://another.url "A funky title"
   [searchengine]: https://www.google.com
```

www.nu.nl

https://google.com

<http://google.com>

<kees@example.com>

[link to this](https://www.google.com)

[like this](http://someurl.com "this title shows up when you hover")

You can also put the [link URL][1] below the current paragraph
like [this][2]. Search the web [here][searchengine]

   [1]: http://url
   [2]: http://another.url "A funky title"
   [searchengine]: https://www.google.com

### Blockquotes

```text
> Blockquotes are very handy in email to emulate reply text.
> This line is part of the same quote.

Quote break.

> This is a very long line that will still be quoted properly when it wraps. Oh boy let's keep writing to make sure this is long enough to actually wrap for everyone. Oh, you can *put* **Markdown** into a blockquote.
```

> Blockquotes are very handy in email to emulate reply text.
> This line is part of the same quote.

Quote break.

> This is a very long line that will still be quoted properly when it wraps. Oh boy let's keep writing to make sure this is long enough to actually wrap for everyone. Oh, you can *put* **Markdown** into a blockquote.

## Code higligting

<pre><code>
Inline `code` has `back-ticks around` it. You can higlight code in a text lik `Get-Service` or a path `smb:\\server1\path`

```javascript
var s = "JavaScript syntax highlighting";
alert(s);
```

```python
def function():
    #indenting works just fine in the fenced code block
    s = "Python syntax highlighting"
    print s
```

```ruby
require 'redcarpet'
markdown = Redcarpet.new("Hello World!")
puts markdown.to_html
```

```
No language indicated, so no syntax highlighting.
s = "There is no highlighting for this."
But let's throw in a <b>tag</b>.
```
<code/></pre>

Inline `code` has `back-ticks around` it. You can higlight code in a text lik `Get-Service` or a path `smb:\\server1\path`

```javascript
var s = "JavaScript syntax highlighting";
alert(s);
```

```python
def function():
    #indenting works just fine in the fenced code block
    s = "Python syntax highlighting"
    print s
```

```ruby
require 'redcarpet'
markdown = Redcarpet.new("Hello World!")
puts markdown.to_html
```

```
No language indicated, so no syntax highlighting.
s = "There is no highlighting for this."
But let's throw in a <b>tag</b>.
```

```txt
Here is an example of AppleScript:

    tell application "Foo"
        beep
    end tell
```

Here is an example of AppleScript:

    tell application "Foo"
        beep
    end tell

## Horizontal Line

```text
---
Line one
***
Line two
___
Line three

you will get a header
---
```

---
Line one

you will get a header
---


## Images

```
Inline-style:
![alt text](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Hover text")

Reference-style:
![alt text][logo]

[logo]: https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 2"
```
Inline-style:
![alt text](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Hover text")

Reference-style:
![alt text][logo1]

[logo1]: https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 2"

```txt
![Hackathon](https://github.com/vjeantet/vjeantet.fr/raw/master/static/images/sgthon/C.jpg?height=80px)
![s](https://github.com/vjeantet/vjeantet.fr/raw/master/static/images/sgthon/C.jpg?classes=border,shadow,)
```
![Hackathon](https://github.com/vjeantet/vjeantet.fr/raw/master/static/images/sgthon/C.jpg?height=80px)
![s](https://github.com/vjeantet/vjeantet.fr/raw/master/static/images/sgthon/C.jpg?classes=border,shadow,)

## Tables

```txt
| header 1 | header 2 |
| -------- | -------- |
| cell 1   | cell 2   |
| cell 3   | cell 4   |
```
| header 1 | header 2 |
| -------- | -------- |
| cell 1   | cell 2   |
| cell 3   | cell 4   |

```txt
First Header  | Second Header
------------- | -------------
Content Cell  | Content Cell
Content Cell  | Content Cell
```
First Header  | Second Header
------------- | -------------
Content Cell  | Content Cell
Content Cell  | Content Cell

The row of dashes between the table header and body must have at least three dashes in each column.

By including colons in the header row, you can align the text within that column:

```txt
| Left Aligned | Centered    | Right Aligned | Left Aligned | Centered | Right Aligned |
| :----------- | :---------: | ------------: | :----------- | :------: | ------------: |
| Cell 1       | Cell 2      | Cell 3        | Cell 4       | Cell 5   | Cell 6        |
| *Cell 7*     | **Cell 8**  | Cell 9        | Cell 10      | Cell 11  | Cell 12       |
```
| Left Aligned | Centered    | Right Aligned | Left Aligned | Centered | Right Aligned |
| :----------- | :---------: | ------------: | :----------- | :------: | ------------: |
| Cell 1       | Cell 2      | Cell 3        | Cell 4       | Cell 5   | Cell 6        |
| *Cell 7*     | **Cell 8**  | Cell 9        | Cell 10      | Cell 11  | Cell 12       |

## Inline escapes and HTML

You can also use raw HTML in your Markdown, and it'll mostly work pretty well.

```html
<dl>
  <dt><b>Definition list</b></dt>
  <dd>Is something people use sometimes.</dd>

  <dt>Markdown in HTML</dt>
  <dd>Does *not* work **very** well. Use HTML <em>tags</em>.</dd>
</dl>
```

<dl>
  <dt><b>Definition list</b></dt>
  <dd>Is something people use sometimes.</dd>

  <dt>Markdown in HTML</dt>
  <dd>Does *not* work **very** well. Use HTML <em>tags</em>.</dd>
</dl>

## Footnote

You can add footnotes to your text as follows.[^2]
[^2]: This is my awesome footnote.

## Backslash Escapes

```txt
\*literal asterisks\*

Markdown provides backslash escapes for the following characters:

\   backslash
`   backtick
*   asterisk
_   underscore
{}  curly braces
[]  square brackets
()  parentheses
#   hash mark
+   plus sign
-   minus sign (hyphen)
.   dot
!   exclamation mark
```

## Task list

```txt
* [x] Completed task
* [ ] Incomplete task
    * [ ] Sub-task 1
    * [x] Sub-task 2
    * [ ] Sub-task 3

1. [x] Completed task
2. [ ] Incomplete task
    1. [ ] Sub-task 1
    2. [x] Sub-task 2
```

* [x] Completed task
* [ ] Incomplete task
    * [ ] Sub-task 1
    * [x] Sub-task 2
    * [ ] Sub-task 3

1. [x] Completed task
1. [ ] Incomplete task
    1. [ ] Sub-task 1
    1. [x] Sub-task 2

# Hugo commands

## Some command memos:

    hugo new site .                      # create new site in current dir
    hugh new post/welcome.md             # create a new post
    hugo server --watch [--buildDrafts]  # Run in server live-reload mode
    hugo                                 # Generate a public directory using default theme
    hugo --theme=my-theme                # ...or with a specific theme
    hugo undraft post/welcome.md         # Un-draft a post (sets draft = "false" in front matter)

## Hugo Shortcodes

## Docdock Shortcode
https://docdock.netlify.com/shortcodes/

# End
