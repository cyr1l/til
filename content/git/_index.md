---
title: "Git"
description : ""
#tags: ["notag"] #[ "Development", "Go", "Powershell", "Blogging" ]
#pre: "<i class='fa fa-github'></i> "
---

## Recources
* [X] https://try.github.io/
* [X] http://rogerdudler.github.io/git-guide/
* http://gitimmersion.com/
* https://git-scm.com/book/en/v2
* http://marklodato.github.io/visual-git-guide/index-en.html
* http://think-like-a-git.net/
* https://learngitbranching.js.org/
* [X] https://docs.gitlab.com/ee/gitlab-basics/README.html
* http://nvie.com/posts/a-successful-git-branching-model/
* https://chris.beams.io/posts/git-commit/ Git commit message guidelines


{{% children depth="999" %}}
