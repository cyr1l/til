---
title: "Basic Config"
date: 2018-01-28T14:16:11+01:00
description : ""
#draft: false
tags: ["git"] #[ "Development", "Go", "Powershell", "Blogging" ]
---

## Basic Config
Git Global config (for local sytem user)


```csharp
git config --global user.name "Gregg Pollack"
git config --global user.email gregg@codeschool.com
git config --global color.ui true
```


## Aliases
Edit the . gitconfig file and add git aliases
In windows you find the . gitconfig in  C:\Program Files\Git\mingw64\etc

```
[alias]
  co = checkout
  ci = commit
  st = status
  br = branch
  hist = log --pretty=format:'%h %ad | %s%d [%an]' --graph --date=short
  type = cat-file -t
  dump = cat-file -p
```
