---
title: "Basics"
date: 2018-01-28T14:16:00+01:00
description : ""
#draft: false
tags: ["git"] #[ "Development", "Go", "Powershell", "Blogging" ]
weight: 1
---


## Working with a local repository
### Create a repository, add files, commit changes

Create a git repository
```
git init
```
Get git status
```
git status
```
Add a file
This will open a text editor(Vim) to save a comment
```
git add file.txt
```
See the status of the repository with
```
git log
```
After a file change it hase to be added again. To place updated file in the staging you have to run
```
git add -u
```
De wijzigingen kunnen opnieuw opgeslagen worden met inline comments
```
git commit -m "Een nieuwe wijziging"
```
Voeg alle nieuwe files toe met
```
git add -A
```
### View history and diffs
View previous changes
```
git log
git log --oneline
git log --summary
git log --graph
```
See shortlog (contains only the username, log summary and changed files number)
```
git shortlog
git shortlog -sne #(s) select summary, (n) order by number of commits, (e) email adresses
```
Zie de verschillen met diffs gebruik hiervoor de hashes(of eerste 6 caracters van de hash). Eerst de oude file..Nieuwe file.
```
git diff 0493c7..9e3dc0
```
Of gebruik HEAD. Dit staat voor de laatste commit versie. Je kunt terug naar een versie hiervoor met HEAD~1
```
git diff HEAD~1..HEAD
of
git diff HEAD~1..
```
### Undo changes
#### For current version
Revert changes of one file. Do a checkout to get the files back from the repository
```
git checkout README.txt
```
Do a reset for multiple changes
```
git reset --hard
```
#### Revert top previous commit
Revert top previous commit. You can not go back to your changed version with this command (easily)
```
git reset --hard HEAD~1
```
## Clean files
Clean files and go back to beginning of commit
What will the clean do?
```
git clean -n
```
Clean the files
```
git clean -f
```
## Ignore files
Add a git ignore file (.gitignore) to the root of the repository or in a subfolder to do a override. You can enter file names or folder like
```
/logs
/logs/*.txt
/*.log
```
## Working with remote repository
### Clone a remote repository
```
https://github.com/jquery/jquery.git
```
